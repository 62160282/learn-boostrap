module.exports = {
  publicPath:
    process.env.NODE_ENV === 'production'
      ? '/~cs62160282/learn_bootstrap/'
      : '/'
}
